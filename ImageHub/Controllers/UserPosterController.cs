﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Reflection.Metadata.Ecma335;
using System.Text;
using System.Threading.Tasks;


namespace ImageHub.Controllers
{
    [ApiController]
    [Route("userposter")]
    public class UserPosterController : ControllerBase
    {
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public ActionResult<String> Create(User user)
        {
            user.Id = user.Id.Substring(0, 7);
            Debug.WriteLine(user.Name);
            Debug.WriteLine(user.Id);
            Debug.WriteLine(user.ProfilePic);
            bool existingUser = false;
            try
            {
                SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();
                builder.DataSource = "imagehub.database.windows.net";
                builder.UserID = "vmarcell";
                builder.Password = "7P3Buz5e";
                builder.InitialCatalog = "ImageHub";

                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();
                    StringBuilder sb = new StringBuilder();
                    sb.Append("SELECT Oauth_uid ");
                    sb.Append("FROM users;");
                    String sql = sb.ToString();

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                if (reader.GetString(0).Equals(user.Id))
                                { existingUser = true; }
                              
                            }
                        }
                    }
                }

                Debug.WriteLine(existingUser);
                if (existingUser == false)
                {
                    using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                    {
                        String query = "INSERT INTO USERS(Oauth_uid, Username, PictureURL, Oauth_provider) VALUES(@id, @username, @pictureUrl, @provider)";


                        using (SqlCommand command = new SqlCommand(query, connection))
                        {
                            command.Parameters.AddWithValue("@id", user.Id);
                            command.Parameters.AddWithValue("@username", user.Name);
                            command.Parameters.AddWithValue("@pictureUrl",user.ProfilePic);
                            command.Parameters.AddWithValue("@provider", "facebook");

                            connection.Open();
                            int result = command.ExecuteNonQuery();

                            // Check Error
                            if (result < 0)
                                Console.WriteLine("Error inserting data into Database!");
                        }
                    }
                }

            }
            catch (SqlException e)
            {
                Console.WriteLine(e.ToString());
            }
          
            return user.Name;
        }
        //[HttpPost]
        //public string Post(string data)
        //{
        //    Debug.WriteLine("TE SANYI");
        //    Debug.WriteLine(data);
        //    // WriteAllLines creates a file, writes a collection of strings to the file,
        //    // and then closes the file.  You do NOT need to call Flush() or Close().
        //    return data;
        //}
    }
}

