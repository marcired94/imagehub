﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Metadata.Ecma335;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Text;
using Microsoft.Extensions.Configuration;
using System.Diagnostics;

namespace ImageHub.Controllers
{
    [ApiController]
    [Route("users")]
    public class UserController : ControllerBase
    {
        //User[] users = new ImageHub.User[100];
        List<User> users = new List<User>();

        public void GetAllUser(string id)
        {
            try
            {
                SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();
                builder.DataSource = "imagehub.database.windows.net";
                builder.UserID = "vmarcell";
                builder.Password = "7P3Buz5e";
                builder.InitialCatalog = "ImageHub";

                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();
                    StringBuilder sb = new StringBuilder();
                    sb.Append("SELECT Username, Oauth_uid, users.PictureURL ");
                    sb.Append("FROM users ");
                    //sb.Append("WHERE Oauth_uid!="+Variables.LoggedInUser.Id+" and ");
                    sb.Append("WHERE Oauth_uid!=" + id + " and ");

                    //sb.Append("users.Oauth_uid not in (select UserIDB from FRIENDS where UserIDA='" + Variables.LoggedInUser.Id + "') and users.Oauth_uid not in (select UserIDA from FRIENDS where UserIDB='" + Variables.LoggedInUser.Id + "')");
                    sb.Append("users.Oauth_uid not in (select UserIDB from FRIENDS where UserIDA=@IDA) and users.Oauth_uid not in (select UserIDA from FRIENDS where UserIDB=@IDA)");

                    String sql = sb.ToString();

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    { //command.Parameters.AddWithValue("@IDA", Variables.LoggedInUser.Id);
                        command.Parameters.AddWithValue("@IDA", id);
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                User adduser = new User();
                                adduser.Name = reader.GetString(0);
                                adduser.Id = reader.GetString(1);
                                adduser.ProfilePic = reader.GetString(2);
                                Debug.WriteLine(adduser.Name + adduser.Id);
                                users.Add(adduser);
                            }
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                Debug.WriteLine(e.ToString());
            }

        }
        [HttpGet]
        public IEnumerable<User> Get()
        {
            string id = Request.Headers["id"];
            id = id.Substring(0,7);
            GetAllUser(id);

            //return Enumerable.Range(1, 99).Select(
            //         index => new User
            //        {
            //            Id = "0",
            //             Name=users[0].Name,
            //            ProfilePic = "000"
            //        }).ToArray();
            return users;


        }
    }
}

