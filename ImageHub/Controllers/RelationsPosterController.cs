﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Reflection.Metadata.Ecma335;
using System.Text;
using System.Threading.Tasks;


namespace ImageHub.Controllers
{
    [ApiController]
    [Route("relationposter")]
    public class RelationsPosterController : ControllerBase
    {
        string UserIDA = "error";
        string UserIDB = "error";
        string id = "0";
        string Status = "error";
        String query = "error";
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public ActionResult<Relation> Create(Relation relation)
        {
            Relation ret = new Relation();
            ret.Id = relation.Id;
            ret.Type = relation.Type;
            ret.UserId = relation.UserId.Substring(0,7);
            Debug.WriteLine(relation.Id);
            Debug.WriteLine(relation.Type);
            Debug.WriteLine("User from relation: " + ret.UserId);
         
            if (ret.Type == "DELETED")
            {
                query = "DELETE from FRIENDS where UserIDA='"+relation.Id+"' and UserIDB='"+ret.UserId+"' and STATUS ='ACCEPTED'";
                sendTheQuery();
                query = "DELETE from FRIENDS where UserIDA='" + ret.UserId + "' and UserIDB='" + relation.Id + "' and STATUS ='ACCEPTED'";
                sendTheQuery();
            }
            if (ret.Type == "ACCEPTED")
            {
                query = "INSERT INTO FRIENDS(ID,UserIDA, UserIDB, FRIENDS.STATUS) VALUES(@id, @UserIDA, @UserIDB, @Status)";
                UserIDA = ret.UserId;
                UserIDB = relation.Id;
                Status = "ACCEPTED";
                int idnum = getID();
                idnum++;
                id = idnum.ToString();
                sendTheQuery();

                query = "INSERT INTO FRIENDS(ID,UserIDA, UserIDB, FRIENDS.STATUS) VALUES(@id, @UserIDA, @UserIDB, @Status)";
                UserIDA = relation.Id;
                UserIDB = ret.UserId;
                Status = "ACCEPTED";
                idnum = getID();
                idnum++;
                id = idnum.ToString();
                sendTheQuery();

                query = "DELETE from FRIENDS where UserIDA='" + relation.Id+ "' and UserIDB='" + ret.UserId + "' and STATUS ='SENTREQUEST'";
                sendTheQuery();
            }
            if (ret.Type == "REJECTED")
            {
                query = "INSERT INTO FRIENDS(ID,UserIDA, UserIDB, FRIENDS.STATUS) VALUES(@id, @UserIDA, @UserIDB, @Status)";
                UserIDA = relation.Id;
                UserIDB = ret.UserId;
                Status = "REJECTED";
                int idnum = getID();
                idnum++;
                id = idnum.ToString();
                sendTheQuery();
            }
            if (ret.Type == "SENTREQUEST")
            {  query = "INSERT INTO FRIENDS(ID,UserIDA, UserIDB, FRIENDS.STATUS) VALUES(@id, @UserIDA, @UserIDB, @Status)";
                UserIDA = ret.UserId;
                UserIDB = relation.Id;
                Status = "SENTREQUEST";
                int idnum = getID();
                idnum++;
                id = idnum.ToString();
                sendTheQuery();
            }
            
        

            return relation;
        }

        private void sendTheQuery()
        {
            try
            {
                SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();
                builder.DataSource = "imagehub.database.windows.net";
                builder.UserID = "vmarcell";
                builder.Password = "7P3Buz5e";
                builder.InitialCatalog = "ImageHub";

                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {


                    using (SqlCommand command = new SqlCommand(query, connection))
                    {
                        command.Parameters.AddWithValue("@id", id);
                        command.Parameters.AddWithValue("@UserIDA", UserIDA);
                        command.Parameters.AddWithValue("@UserIDB", UserIDB);
                        command.Parameters.AddWithValue("@Status", Status);

                        connection.Open();
                        int result = command.ExecuteNonQuery();

                        // Check Error
                        if (result < 0)
                            Console.WriteLine("Error inserting data into Database!");
                    }
                }


            }
            catch (SqlException e)
            {
                Debug.WriteLine(e.ToString());
            }
        }

        private int getID()
        {
            int retid = 0;

            try
            {
                SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();
                builder.DataSource = "imagehub.database.windows.net";
                builder.UserID = "vmarcell";
                builder.Password = "7P3Buz5e";
                builder.InitialCatalog = "ImageHub";

                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();
                    StringBuilder sb = new StringBuilder();
                    sb.Append("select top 1 ID from friends order by id desc;");
                    String sql = sb.ToString();
                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                retid = int.Parse(reader.GetDecimal(0).ToString());
                          
                            }
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                Debug.WriteLine(e.ToString());
            }
            return retid;
        }
    }
        //[HttpPost]
        //public string Post(string data)
        //{
        //    Debug.WriteLine("TE SANYI");
        //    Debug.WriteLine(data);
        //    // WriteAllLines creates a file, writes a collection of strings to the file,
        //    // and then closes the file.  You do NOT need to call Flush() or Close().
        //    return data;
        //}
    
}

