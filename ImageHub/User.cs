using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace ImageHub
{
    public class User
    {
        public string Name { get; set; }
        public string ProfilePic { get; set; }

        public string Id { get; set; }
       // public string ConnectionType { get; set; } //accepted, not accepted
    }
}
