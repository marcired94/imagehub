import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ConstantsService {
    public userName;
    public userProfilePicUrl;
    public userId;
  constructor() { }
}
