import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
    selector: 'app-logout-component',
    templateUrl: './logout.component.html'
})
export class LogoutComponent {
    constructor(private http: HttpClient) {
        window.location.href = "https://localhost:44384/";
    }
}
