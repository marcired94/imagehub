import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { NavMenuComponent } from './nav-menu/nav-menu.component';
import { HomeComponent } from './home/home.component';
import { ProfileComponent } from './profile/profile.component';
import { LogoutComponent } from './logout/logout.component';
import { PostComponent } from './post/post.component';
import { AuthServiceConfig, FacebookLoginProvider, SocialLoginModule } from "angularx-social-login";
import { Observable } from 'rxjs'
import { ConstantsService } from './common/services/constants.service';
import { FriendsComponent } from './friends/friends.component';


const config = new AuthServiceConfig([
    {
        id: FacebookLoginProvider.PROVIDER_ID,
        provider: new FacebookLoginProvider("397413654497388")
    }
]);

export function provideConfig() {
    return config;
}

@NgModule({
    declarations: [
        AppComponent,
        NavMenuComponent,
        HomeComponent,
        ProfileComponent,
        LogoutComponent,
        PostComponent,
        FriendsComponent,

    ],
    imports: [
        BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
        SocialLoginModule,
        HttpClientModule,
        FormsModule,
        RouterModule.forRoot([
            { path: '', component: HomeComponent, pathMatch: 'full' },
            { path: 'home', component: HomeComponent },
            { path: 'profile', component: ProfileComponent },
            { path: 'post', component: PostComponent },
            { path: 'logout', component: LogoutComponent },
            { path: 'friends', component: FriendsComponent },

        ])
    ],
    providers: [
        {
            provide: AuthServiceConfig,
            useFactory: provideConfig
        }, ConstantsService],
    bootstrap: [AppComponent]
})
export class AppModule { }
