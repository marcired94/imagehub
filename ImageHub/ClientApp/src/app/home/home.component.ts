import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.css']
})
export class HomeComponent {
   
    public imagePath;
    imgURL: any;
    public message: string;

    preview(files) {
        if (files.length === 0)
            return;

        var mimeType = files[0].type;
        if (mimeType.match(/image\/*/) == null) {
            this.message = "Only images are supported.";
            return;
        }

        var reader = new FileReader();
        this.imagePath = files;
        reader.readAsDataURL(files[0]);
        reader.onload = (_event) => {
            this.imgURL = reader.result;
        }
    }

    fileData: File = null;
    constructor(private http: HttpClient) {
    }

    fileProgress(fileInput: any) {
        this.fileData = <File>fileInput.target.files[0];
    }

    onSubmit() {
        const formData = new FormData();
        formData.append('file', this.fileData);
        this.http.post('url/to/your/api', formData)
            .subscribe(res => {
                console.log(res);
                alert('SUCCESS !!');
            })
    }
}


