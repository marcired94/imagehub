import { Component, Inject } from '@angular/core';
import { AuthService, SocialUser, FacebookLoginProvider } from "angularx-social-login";
import { ConstantsService } from './common/services/constants.service';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';

export interface datauser{
    name: string
}
interface IUser {
    name: string;
    profilePic: string;
    id: string;
}

class User implements IUser {
    name: string;
    profilePic: string;
    id: string;
}
@Component({
    selector: 'app-root',
    templateUrl: './app.component.html'
})
export class AppComponent {
    title = 'app';
    login: boolean = false;
    result: User;

    loginClick() {
        this.login = true;
        this._constant.userName = this.user.name;
        this._constant.userProfilePicUrl = this.user.photoUrl;
        this._constant.userId = this.user.id;
        this.sendToDb();
    }
    sendToDb() {
        const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
        let options = { headers: headers };
        // this.httpClient.post<User>(baseUrl + "userposter", JSON.stringify({ Name: "SANYI", Id: "000", ProfilePic: "qwer" }));
        this.httpClient.post<User>("https://localhost:44384/userposter", JSON.stringify({ Name: this.user.name, Id: this.user.id, ProfilePic: this.user.photoUrl }), options).subscribe(result => {
            this.result = result;
        }, error => console.error(error));
        console.error(this.result.name)
    }
   
    public user: any = SocialUser;
    constructor(private socialAuthService: AuthService, private _constant: ConstantsService, private httpClient: HttpClient) { }
    facebookLogin() {
        const dbuser = new User();
      //Eternal thanks for Lokesh Kumar
        this.socialAuthService.signIn(FacebookLoginProvider.PROVIDER_ID).then((userData) =>
            this.user = userData);
        dbuser.name = this.user.name;
        dbuser.id = this.user.id;
        dbuser.profilePic = this.user.photoUrl;
     

    }
}
