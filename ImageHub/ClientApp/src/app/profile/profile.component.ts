import { Component } from '@angular/core';
import { ConstantsService } from '../common/services/constants.service';

@Component({
  selector: 'app-profile',
    templateUrl: './profile.component.html',
    styleUrls: ['./profile.component.css']

})
export class ProfileComponent {
    name: string;
    photoUrl: string;
    constructor(private _constant: ConstantsService) {
        this.name = this._constant.userName;
        this.photoUrl = this._constant.userProfilePicUrl;
    }
    
}
