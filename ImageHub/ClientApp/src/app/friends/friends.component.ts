import { Component,Inject } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { ConstantsService } from '../common/services/constants.service';


@Component({
  selector: 'app-friends-component',
  templateUrl: './friends.component.html'
})
export class FriendsComponent {
    public users: User[];
    public friends: User[];
    public wannabes: User[];
    public resultOfPost: Relation;
    buttonClicked(user: User, type: string) {
        const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
        let options = { headers: headers };
        this.http.post<Relation>("https://localhost:44384/relationposter", JSON.stringify({ Id: user.id, Type: type, UserId: this._constant.userId }), options).subscribe(result => {
            this.resultOfPost = result;
        }, error => console.error(error));
        console.error(this.resultOfPost)
        
    }
    reloadDB()
    {
        let baseurl = "https://localhost:44384/";
        let headers = new HttpHeaders().set('id', this._constant.userId);
        let params = new HttpParams().set("id", this._constant.userId);

        this.http.get<User[]>(baseurl + 'users', { headers: headers, params: params }).subscribe(result => {
            this.users = result;
        }, error => console.error(error));
        this.http.get<User[]>(baseurl + 'friends', { headers: headers, params: params }).subscribe(result => {
            this.friends = result;
        }, error => console.error(error));
        this.http.get<User[]>(baseurl + 'wannabes', { headers: headers, params: params }).subscribe(result => {
            this.wannabes = result;
        }, error => console.error(error));
    }
    constructor(private http: HttpClient, @Inject('BASE_URL') baseUrl: string, private _constant: ConstantsService) {
        let headers = new HttpHeaders().set('id', _constant.userId);
        let params = new HttpParams().set("id", _constant.userId); 

        http.get<User[]>(baseUrl + 'users', { headers: headers, params: params }).subscribe(result => {
            this.users = result;
        }, error => console.error(error));
        http.get<User[]>(baseUrl + 'friends', { headers: headers, params: params }).subscribe(result => {
            this.friends = result;
        }, error => console.error(error));
        http.get<User[]>(baseUrl + 'wannabes',{ headers: headers, params: params }).subscribe(result => {
            this.wannabes = result;
        }, error => console.error(error));
    }

   
}


interface User {
    name: string;
    profilePic: string;
    id: string;
    connectiontype: string;
}
class Relation {
    id: string;
    type: string;
    userid: string;
}
