using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace ImageHub
{
    public class Relation
    {
        public string Id { get; set; }
        public string Type { get; set; }
        public string UserId { get; set; }
    }
}
